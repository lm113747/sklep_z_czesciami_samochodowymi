package com.example.project_car_parts.Repository;

import com.example.project_car_parts.Model.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order,Long> {
}
