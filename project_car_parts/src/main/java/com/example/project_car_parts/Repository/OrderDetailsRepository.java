package com.example.project_car_parts.Repository;

import com.example.project_car_parts.Model.OrderDetails;
import org.springframework.data.repository.CrudRepository;

public interface OrderDetailsRepository extends CrudRepository<OrderDetails, Long> {
}
