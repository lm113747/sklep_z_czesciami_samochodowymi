package com.example.project_car_parts.Repository;

import com.example.project_car_parts.Model.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {
}
