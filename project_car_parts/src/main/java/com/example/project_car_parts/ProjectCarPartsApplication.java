package com.example.project_car_parts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectCarPartsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectCarPartsApplication.class, args);
	}

}
