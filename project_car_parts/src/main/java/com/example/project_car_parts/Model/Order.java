package com.example.project_car_parts.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Date;

@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id")
    private Long id;
    private Instant orderDate;



    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "emp_id", referencedColumnName = "employee_id")
    private Employee employees;

    @OneToOne(fetch = FetchType.LAZY , cascade = CascadeType.ALL)
    @JoinColumn(name = "orderDetails_id", referencedColumnName = "id")
    private OrderDetails orderDetails;

    public Employee getEmployee() {
        return employees;
    }

    public Order() {
    }

    public Order(Instant orderDate) {
        this.orderDate = orderDate;


    }

    public Instant getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Instant orderDate) {
        this.orderDate = orderDate;
    }


    public OrderDetails getOrderDetails() {
        return orderDetails;
    }


    public void setOrderDetails(OrderDetails orderDetails) {
        this.orderDetails = orderDetails;
    }

    public void setEmployee(Employee employee) {
        this.employees = employee;
    }

}
