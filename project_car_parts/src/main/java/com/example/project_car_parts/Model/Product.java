package com.example.project_car_parts.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;

import javax.persistence.*;

@Entity
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private float nr_seryjny;
    private String nazwa;
    private String marka;
    private BigDecimal cena;
    private int ilosc;


    @Transient
    @OneToOne(mappedBy = "product")
    private OrderDetails orderDetails;

    public Product() {
    }

    public Product(float nr_seryjny, String nazwa, String marka, BigDecimal cena, int ilosc) {
        this.nr_seryjny = nr_seryjny;
        this.nazwa = nazwa;
        this.marka = marka;
        this.cena = cena;
        this.ilosc = ilosc;
    }

    public Product(Long id, float nr_seryjny, String nazwa, String marka, BigDecimal cena) {
        this.id = id;
        this.nr_seryjny = nr_seryjny;
        this.nazwa = nazwa;
        this.marka = marka;
        this.cena = cena;

    }

    public Product(Long id) {
        this.id = id;
    }

    public Product(String nazwa){
        this.nazwa = nazwa;
    }




    public float getNr_seryjny() {
        return nr_seryjny;
    }

    public void setNr_seryjny(float nr_seryjny) {
        this.nr_seryjny = nr_seryjny;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public BigDecimal getCena() {
        return cena;
    }

    public void setCena(BigDecimal cena) {
        this.cena = cena;
    }

    public int getIlosc() {
        return ilosc;
    }

    public void setIlosc(int ilosc) {
        this.ilosc = ilosc;
    }

    @JsonIgnore
    public OrderDetails getOrderDetails() {
        return orderDetails;
    }
    @JsonIgnore
    public void setOrderDetails(OrderDetails orderDetails) {
        this.orderDetails = orderDetails;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
