package com.example.project_car_parts.Controller;

import com.example.project_car_parts.Model.Product;
import com.example.project_car_parts.Repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ProductController {

    ProductRepository productRepository;

    Logger logger = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    @GetMapping("/products")
    public List<Product> getAllProducts() {

        List<Product> lista  = new ArrayList<>();


        productRepository.findAll().forEach(product -> lista.add(product));

        return lista;
    }

    @PostMapping("/saveProduct")
    public void saveProduct(@RequestBody Product product ) {

        productRepository.save(new Product(product.getNr_seryjny(),product.getNazwa(),product.getMarka(),product.getCena(),product.getIlosc()));

    }

    @PostMapping("/changeProductQuantity")
    public void changeProductQuantity(@RequestParam(name = "quantity") int quantity, @RequestParam(name = "serial_number") int serial_number){

        productRepository.findAll().forEach(product -> {
            if(product.getNr_seryjny() == serial_number){
                product.setIlosc(product.getIlosc() - quantity);
                productRepository.save(product);
            }
        });

        logger.info(String.valueOf(quantity));
        logger.info(String.valueOf(serial_number));

    }

    @DeleteMapping("/deleteProductBySerialNumber")
    public void deleteProductBySerialNumber(@RequestParam(name = "serial_number") float serial_number){

        productRepository.findAll().forEach(product -> {
            if (product.getNr_seryjny() == serial_number){
                productRepository.delete(product);
            }
        });

    }

}
