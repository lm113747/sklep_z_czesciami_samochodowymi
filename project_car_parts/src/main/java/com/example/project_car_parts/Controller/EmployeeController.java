package com.example.project_car_parts.Controller;

import com.example.project_car_parts.Model.Employee;
import com.example.project_car_parts.Repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class EmployeeController {

    EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeController(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @PostMapping("/addEmployee")
    public void addEmployee(@RequestParam(name = "userName") String name, @RequestParam(name = "userSurname") String surname){

        Employee employee = new Employee(name,surname);

        employeeRepository.save(employee);

    }


}
