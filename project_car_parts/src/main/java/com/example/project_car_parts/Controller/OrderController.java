package com.example.project_car_parts.Controller;

import com.example.project_car_parts.Model.Employee;
import com.example.project_car_parts.Model.Order;
import com.example.project_car_parts.Model.OrderDetails;
import com.example.project_car_parts.Model.Product;
import com.example.project_car_parts.Repository.EmployeeRepository;
import com.example.project_car_parts.Repository.OrderDetailsRepository;
import com.example.project_car_parts.Repository.OrderRepository;
import com.example.project_car_parts.Repository.ProductRepository;
import org.aspectj.runtime.reflect.Factory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.xml.crypto.Data;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAmount;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@RestController
@RequestMapping("/api")
public class OrderController {

    OrderRepository orderRepository;
    OrderDetailsRepository orderDetailsRepository;
    ProductRepository productRepository;
    EmployeeRepository employeeRepository;

    Logger logger = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    public OrderController(OrderRepository orderRepository, OrderDetailsRepository orderDetailsRepository,
                           ProductRepository productRepository, EmployeeRepository employeeRepository) {
        this.orderRepository = orderRepository;
        this.orderDetailsRepository = orderDetailsRepository;
        this.productRepository = productRepository;
        this.employeeRepository = employeeRepository;

    }

    @GetMapping("/getOrder")
    public List<Order> getOrder(@RequestParam(name = "userId") Long userId ) {


        List<Order> lista = new ArrayList<>();

        orderRepository.findAll().forEach(order -> {


            if (order.getEmployee().getId().equals(userId)) {
                Order ord = new Order();
                OrderDetails orderDetails = new OrderDetails();


                orderDetails.setMessage(order.getOrderDetails().getMessage());
                orderDetails.setProduct(new Product(order.getOrderDetails().getProduct().getId(),order.getOrderDetails().getProduct().getNr_seryjny(), order.getOrderDetails().getProduct().getNazwa(), order.getOrderDetails().getProduct().getMarka(),order.getOrderDetails().getProduct().getCena()));
                orderDetails.setQuantity(order.getOrderDetails().getQuantity());

                ord.setEmployee(new Employee(order.getEmployee().getId(),order.getEmployee().getName(), order.getEmployee().getSurname()));


                orderDetails.setStatus(order.getOrderDetails().getStatus());
                ord.setOrderDate(order.getOrderDate().plus(2L, ChronoUnit.HOURS));
                ord.setOrderDetails(orderDetails);


                lista.add(ord);

            }

        });

        return lista;

    }

    @PostMapping("/addOrder")
    public void addOrder(@RequestParam(name = "quantity") int quantity,
                         @RequestParam(name = "serial_number") int serial_number,
                         @RequestParam(name = "message") String message,
                         @RequestParam(name = "userId") Long id) {


        Order order = new Order(Instant.now());
        OrderDetails orderDetails = new OrderDetails(quantity, message);

        Product product = new Product();

        List<Product> ls = new ArrayList<>();
        List<Employee> l = new ArrayList<>();

        employeeRepository.findAll().forEach(employee -> {
            if (employee.getId().equals(id)) {
                l.add(employee);
            }
        });

        productRepository.findAll().forEach(prod -> {

            if (prod.getNr_seryjny() == serial_number) {

                ls.add(prod);
            }
        });


        l.get(0).setOrder(order);

        orderDetails.setProduct(ls.get(0));
        product.setOrderDetails(orderDetails);
        orderDetails.setStatus(OrderDetails.Status.OCZEKUJACY);

        order.setOrderDetails(orderDetails);
        order.setEmployee(l.get(0));
        orderDetails.setOrder(order);


        orderRepository.save(order);


    }


}
